<?php

namespace App\HttpController;
use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\EasySwoole\Config;
use think\Template;

/**
 * 带视图的控制器基类
 * Class DbViewController
 * @author  : 李梓钿
 * @package App
 */
abstract class ViewController extends Controller
{
    protected $view;		
	
	function onRequest(?string $action): ?bool
    {				
		$this->view             = new Template();
        $tempPath               = Config::getInstance()->getConf('TEMP_DIR');     # 临时文件目录
		
		$class_name_array       = explode('\\', static::class);
        $class_name_array_count = count($class_name_array);
        $controller_path= $class_name_array[$class_name_array_count - 2] . '/' . $class_name_array[$class_name_array_count - 1] . '/';
		
		$templatec_config=Config::getInstance()->getConf('template');
		
        $this->view->config([
			'view_path' => EASYSWOOLE_ROOT . '/Views/'. $controller_path,		
			'cache_path' => "{$tempPath}/templates_c/",               # 模板编译目录
			"tpl_replace_string"=>$templatec_config['view_replace_str'],
			'default_filter'=>''
		]);
		
		$this->session()->start();
		
		// 控制器初始化
        $this->_initialize();
		
		return parent::onRequest($action);
	}
	
	/**
     * 初始化操作
     * @access protected
     */
    protected function _initialize()
    {
    }
	
    public function afterAction(?string $actionName): void
    {
        $this->view = null;
        parent::afterAction($actionName); 
    }

    /**
     * 输出模板到页面
     * @param  string|null $template 模板文件
     * @param array $vars 模板变量值
     * @param array $config 额外的渲染配置
     * @author : 李梓钿
     */
    protected function fetch($template, $vars = [], $config = [])
    {
       ob_start();
       $this->view->fetch($template, $vars, $config);
       $content = ob_get_clean();
       $this->response()->write($content);		
    }

    /**
     * 模板变量赋值
     * @access public
     * @param mixed $name
     * @param mixed $value
     * @return void
     */
    protected function assign($name, $value = '')
    {
        $this->view->assign($name, $value);		
    }
	
	//返回json数据
	protected function send($data = [])
	{
		$this->response()->withAddedHeader( 'Access-Control-Allow-Origin', Config::getInstance()->getConf( 'response.access_control_allow_origin' ) );
		$this->response()->withAddedHeader( 'Content-Type', 'application/json; charset=utf-8' );
		$this->response()->withAddedHeader( 'Access-Control-Allow-Headers', Config::getInstance()->getConf( 'response.access_control_allow_headers' ) );
		$this->response()->withAddedHeader( 'Access-Control-Allow-Methods', Config::getInstance()->getConf( 'response.access_control_allow_methods' ) );

		$this->response()->withStatus( 200);

		
		$this->response()->write( json_encode( $data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) );
		$this->response()->end();
	}
	
	//获取post数据
	public function post(){
		
		$data=$this->request()->getParsedBody();	
		
		return sys_filter($data);
	}
	//获取get数据
	public function get(){
		
		$data=$this->request()->getQueryParams();		
		
		return sys_filter($data);
	}
}