<?php

namespace App\HttpController\Admin;
use App\HttpController\AdminController;

class User extends AdminController
{	
	protected function _initialize() {
		
		parent::_initialize();
		$this->assign(['breadcrumb1'=>'系统']);
		$this->assign(['breadcrumb2'=>'系统用户']);		
	}

	function index(){
		$param=$this->get();
		
		$list=osc_model('user','admin')->get_page($param);		
		
		$this->fetch('index',[
			'param'=>$param,
			'empty'=>'<tr><td colspan="20">没有数据~</td></tr>',
			'list'=>$list['list'],
			'page_render'=>$list['page'],
			'total'=>$list['total']
		]);
	}
	
	function add()
    {	

    	 if($post=$this->post()){
			
			$validate=osc_validate('admin','admin')->validate($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
			
			$r=osc_model('user','admin')->add_admin($post);
			
			if(isset($r['error'])){
				return $this->send($r);
			}else{
				return $this->send(['success'=>'新增成功','action'=>'add']);
			}
			
			
		}

        $this->assign([
			'crumbs'=>'新增',
			'action'=>'/admin/user/add',
			'group'=>osc_model('user','admin')->get_all_group()
		]);
        $this->fetch('edit');
    }
	
    function edit()
    {
        if($post=$this->post()){
			
			$r=osc_model('user','admin')->edit_admin($post);
			
			if(isset($r['error'])){
				return $this->send($r);
			}else{
				return $this->send(['success'=>'修改成功','action'=>'edit']);
			}
		}

		$param=$this->get();

        $this->assign([
			'crumbs'=>'修改',
			'action'=>'/admin/user/edit',
			'group'=>osc_model('user','admin')->get_all_group(),
			'user'=>osc_model('user','admin')->getById($param['id']),
			'id'=>$param['id']
		]);
        $this->fetch('edit');
    }

    function set_status(){

    	osc_model('user','admin')->set_status($this->get());

    	$this->response()->redirect('/admin/user/index');	
    }

	function pwd()
    {	
    	if($post=$this->post()){
			
    		$user=$this->session()->get('user_auth');

    		$post['uid']=$user['uid'];

    		$validate=osc_validate('admin','admin')->pwd($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
    		
			$r=osc_model('user','admin')->update_pwd($post);
			
			if(isset($r['error'])){
				return $this->send($r);
			}else{
				return $this->send(['success'=>'修改成功','action'=>'edit']);
			}
		}

        $this->assign(['crumbs'=>'修改']);
        $this->fetch('pwd');
    }

    function del(){
		
		$get=$this->get();

		$id=(int)$get['id'];
		
		if(osc_model('user','admin')->delete($id)){
			
			return $this->send(['url'=>'/admin/user/index']);

		}	
		
	}


}