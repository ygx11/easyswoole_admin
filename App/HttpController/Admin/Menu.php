<?php

namespace App\HttpController\Admin;
use App\HttpController\AdminController;

class Menu extends AdminController
{	
	protected function _initialize() {
		
		parent::_initialize();
		$this->assign(['breadcrumb1'=>'系统']);
		$this->assign(['breadcrumb2'=>'后台菜单管理']);		
	}

	function index(){
				
		$list=osc_model('admin','Menu')->get_all_menu();		
		
		$this->fetch('index',['list'=>json_encode(osc_tools('tree')->list_to_tree($list))]);
	}	
    
	function add()
    {
        if($post=$this->post()){
			
			$validate=osc_validate('admin','menu')->validate($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
			
			$r=osc_model('admin','Menu')->add($post);
			
			if(isset($r['error'])){
				return $this->send($r);
			}else{
				return $this->send(['success'=>'新增成功','id'=>$r,'name'=>$post['title']]);
			}
			
			
		}
		
		
    }
	function edit()
    {
        if($post=$this->post()){
			
			$validate=osc_validate('admin','menu')->validate($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
						
			if(osc_model('admin','Menu')->edit($post)){
												
				return $this->send(['success'=>'编辑成功','name'=>$post['title']]);
			}else{
				return $this->send(['error'=>'编辑失败']);
			}
		
			
		}
     
    }
	function del(){
		if($post=$this->post()){
			$id=(int)$post['id'];
			
			if(osc_model('admin','Menu')->get_by_pid($id)){				
				return $this->send(['error'=>'请先删除子节点']);
			}
			
			if(osc_model('admin','Menu')->delete($id)){
												
				return $this->send(['success'=>'删除成功']);
			}			
		}
	}
	function get_info(){
		if($post=$this->post()){
			
			$id=(int)$post['id'];
			
			$d=osc_model('admin','Menu')->get_by_id($id);
			
			return $this->send(['title'=>$d['title'],'url'=>$d['url'],'type'=>$d['type'],'icon'=>$d['icon'],'module'=>$d['module'],'sort_order'=>$d['sort_order']]);
		}
	}
}