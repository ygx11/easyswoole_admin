<?php

namespace App\Model\User;
use App\Model\BaseModel;
use App\Utility\Pagination;
use App\Utility\Encrypt;

class Admin extends BaseModel
{
    protected $table = 'osc_admin';

    function getById($uid){
        $data = $this->db->where('admin_id', $uid)->getOne($this->table);
        return empty($data) ? null : $data;
    }
	
	function getByName($name){
        $data = $this->db->where('user_name', $name)->getOne($this->table);
        return empty($data) ? null : $data;
    }  
	
	function get_all_group(){
		$data = $this->db->get('osc_admin_auth_group',null,'id,title');
        return empty($data) ? null : $data;
	}
	
	function get_page($param){			
	
		$page=!isset($param['page']) ? 0 : $param['page'];
		
		if($page!=0){			
			$get_page=$page-1;
		}else{
			$get_page=0;
		}
		
		$page_size=DbConfig('page_num');
		
		$sql='select ag.title,a.admin_id,a.user_name,a.status,aga.group_id from '.$this->table.' as a,osc_admin_auth_group_access as aga,osc_admin_auth_group as ag'
		." where a.admin_id=aga.uid and aga.group_id=ag.id and user_name!='".DbConfig('administrator')."' order by a.admin_id desc";
		
		$total=count($this->db->rawQuery($sql));
		
		$sql.=" limit ".$get_page*$page_size.','.$page_size;
		
		$list=null;
		
		$list = $this->db->rawQuery($sql);
		 
		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $page_size;
		$pagination->param = $param;
		$pagination->url ='/admin/user/index';
		
		$render = $pagination->render();		 
		 
		return ['list'=>$list,'page'=>$render,'total'=>$total];
		 
	}

	function add_admin($data){
		
		$admin['user_name']=$data['user_name'];
		$admin['true_name']=$data['true_name'];
		$admin['telephone']=$data['telephone'];
		$admin['status']=$data['status'];
		//dmin['wechat_openid']=trim($data['wechat_openid']);
		$admin['create_time']=time();		
		$admin['group_id']=$data['group_id'];		
		$admin['salt']=Encrypt::get_salt();			
		$admin['passwd']=Encrypt::encode_password($data['passwd'],$admin['salt']);
		
		if($admin_id= $this->db->insert($this->table,$admin)){
			
			$this->db->insert('osc_admin_auth_group_access',['uid'=>$admin_id,'group_id'=>$data['group_id']]);

		}else{
			return ['error'=>$this->db->getLastError()];
		}

	}

	function edit_admin($data){

		$admin['user_name']=trim($data['user_name']);				
		$admin['true_name']=$data['true_name'];
		$admin['telephone']=$data['telephone'];
		$admin['status']=$data['status'];		
		$admin['group_id']=$data['group_id'];
		
		if(!empty($data['passwd'])){
			$admin['salt']=Encrypt::get_salt();			
			$admin['passwd']=Encrypt::encode_password($data['passwd'],$admin['salt']);
		}		
		$admin['update_time']=time();

		if($this->db->where('admin_id',$data['admin_id'])->update($this->table,$admin)){
			$this->db->where('uid',$data['admin_id'])->update('osc_admin_auth_group_access',['group_id'=>$data['group_id']]);
		}else{
			return ['error'=>$this->db->getLastError()];
		}
	}

	function set_status($data){		
		$this->db->where('admin_id',$data['id'])->update($this->table,['status'=>$data['status']]);		
	}

	function delete($id){
        $this->db->where('admin_id',$id)->delete('osc_admin_auth_group_access');
        $this->db->where('uid',$id)->delete($this->table);
        return true;
    }
    function update_pwd($data){
    	
    	$admin['salt']=Encrypt::get_salt();			
		$admin['passwd']=Encrypt::encode_password($data['passwd'],$admin['salt']);
		$admin['update_time']=time();

		if(!$this->db->where('admin_id',$data['uid'])->update($this->table,$admin)){
		
			return ['error'=>$this->db->getLastError()];
		}
    }
}