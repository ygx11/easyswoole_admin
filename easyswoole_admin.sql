-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2019-03-31 17:00:34
-- 服务器版本： 5.7.24-log
-- PHP 版本： 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `oscshop_easyswoole`
--

-- --------------------------------------------------------

--
-- 表的结构 `osc_admin`
--

CREATE TABLE `osc_admin` (
  `admin_id` mediumint(8) NOT NULL,
  `user_name` varchar(20) DEFAULT NULL COMMENT '用户名',
  `salt` varchar(20) DEFAULT NULL,
  `passwd` varchar(128) DEFAULT NULL,
  `true_name` varchar(20) DEFAULT NULL COMMENT '真名',
  `telephone` varchar(40) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `wechat_openid` varchar(100) DEFAULT NULL,
  `login_count` mediumint(8) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `last_login_ip` varchar(40) DEFAULT NULL COMMENT '最后登录ip',
  `last_ip_region` varchar(40) DEFAULT NULL,
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT '0',
  `last_login_time` int(10) NOT NULL DEFAULT '0' COMMENT '最后登录',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `group_id` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台管理员';

--
-- 转存表中的数据 `osc_admin`
--

INSERT INTO `osc_admin` (`admin_id`, `user_name`, `salt`, `passwd`, `true_name`, `telephone`, `email`, `wechat_openid`, `login_count`, `last_login_ip`, `last_ip_region`, `create_time`, `update_time`, `last_login_time`, `status`, `group_id`) VALUES
(1, 'admin', '4ijolb', '6f71084f7f748cf7b7526b72ec4aedd2', '', '', NULL, '', 84, '127.0.0.1', NULL, 1536807926, 1554022756, 1552743815, 1, 2),
(15, 'test', 'a2xpqk', '0ef33c8ebda92259676fc36c0f836216', '', '', NULL, NULL, 0, NULL, NULL, 1554017875, 0, 0, 1, 2);

-- --------------------------------------------------------

--
-- 表的结构 `osc_admin_auth_group`
--

CREATE TABLE `osc_admin_auth_group` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT '用户组id,自增主键',
  `title` char(20) DEFAULT NULL COMMENT '用户组中文名称',
  `description` varchar(80) DEFAULT NULL COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` text COMMENT '用户组拥有的规则id，多个规则 , 隔开'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `osc_admin_auth_group`
--

INSERT INTO `osc_admin_auth_group` (`id`, `title`, `description`, `status`, `rules`) VALUES
(2, '测试', '测试', 1, '1,2,46,120,121,122,123,197,198,199,445');

-- --------------------------------------------------------

--
-- 表的结构 `osc_admin_auth_group_access`
--

CREATE TABLE `osc_admin_auth_group_access` (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户组id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `osc_admin_auth_group_access`
--

INSERT INTO `osc_admin_auth_group_access` (`uid`, `group_id`) VALUES
(15, 2);

-- --------------------------------------------------------

--
-- 表的结构 `osc_admin_auth_rule`
--

CREATE TABLE `osc_admin_auth_rule` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT '规则id,自增主键',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `osc_admin_auth_rule`
--

INSERT INTO `osc_admin_auth_rule` (`id`, `group_id`, `menu_id`, `name`) VALUES
(69, 2, 1, 'admin/settings/general'),
(70, 2, 2, 'admin/menu/index'),
(71, 2, 46, 'admin/config/index'),
(72, 2, 120, 'admin/menu/add'),
(73, 2, 121, 'admin/menu/edit'),
(74, 2, 122, 'admin/menu/del'),
(75, 2, 123, 'admin/menu/get_info'),
(76, 2, 197, 'admin/config/add'),
(77, 2, 198, 'admin/config/edit'),
(78, 2, 199, 'admin/config/del'),
(79, 2, 445, 'admin/index/index');

-- --------------------------------------------------------

--
-- 表的结构 `osc_admin_menu`
--

CREATE TABLE `osc_admin_menu` (
  `id` int(10) UNSIGNED NOT NULL COMMENT '文档ID',
  `module` varchar(20) DEFAULT NULL,
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `url` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `icon` varchar(64) DEFAULT NULL,
  `sort_order` int(10) DEFAULT '0' COMMENT '排序（同级有效）',
  `type` varchar(40) DEFAULT NULL COMMENT 'nav,auth',
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台菜单';

--
-- 转存表中的数据 `osc_admin_menu`
--

INSERT INTO `osc_admin_menu` (`id`, `module`, `pid`, `title`, `url`, `icon`, `sort_order`, `type`, `status`) VALUES
(1, 'admin', 0, '系统', 'admin/settings/general', 'fa-tachometer', 7, 'nav', 1),
(2, 'admin', 1, '后台菜单管理', 'admin/menu/index', '', 10, 'nav', 1),
(46, 'admin', 1, '配置管理', 'admin/config/index', '', 5, 'nav', 1),
(120, 'admin', 2, '新增', 'admin/menu/add', '', 1, 'auth', 1),
(121, 'admin', 2, '编辑', 'admin/menu/edit', '', 2, 'auth', 1),
(122, 'admin', 2, '删除', 'admin/menu/del', '', 3, 'auth', 1),
(123, 'admin', 2, '获取信息', 'admin/menu/get_info', '', 4, 'auth', 1),
(125, 'admin', 1, '权限管理', 'admin/auth/index', '', 2, 'nav', 1),
(127, 'admin', 1, '系统用户', 'admin/user/index', '', 4, 'nav', 1),
(187, 'admin', 125, '新增用户组', 'admin/auth/create_group', '', 0, 'auth', 1),
(188, 'admin', 125, '编辑用户组', 'admin/auth/edit_group', '', 0, 'auth', 1),
(189, 'admin', 125, '删除用户组', 'admin/auth/del_group', '', 0, 'auth', 1),
(190, 'admin', 125, '查看权限', 'admin/auth/access', '', 0, 'auth', 1),
(191, 'admin', 125, '编辑权限', 'admin/auth/write_group', '', 0, 'auth', 1),
(192, 'admin', 125, '设置状态', 'admin/auth/set_status', '', 0, 'auth', 1),
(193, 'admin', 127, '新增', 'admin/user/add', '', 0, 'auth', 1),
(194, 'admin', 127, '编辑', 'admin/user/edit', '', 0, 'auth', 1),
(195, 'admin', 127, '删除', 'admin/user/del', '', 0, 'auth', 1),
(196, 'admin', 127, '设置状态', 'admin/user/set_status', '', 0, 'auth', 1),
(197, 'admin', 46, '新增', 'admin/config/add', '', 0, 'auth', 1),
(198, 'admin', 46, '编辑', 'admin/config/edit', '', 0, 'auth', 1),
(199, 'admin', 46, '删除', 'admin/config/del', '', 0, 'auth', 1),
(290, 'admin', 57, '退出系统', 'admin/index/logout', '', 1, 'auth', 1),
(445, 'admin', 0, '首页', 'admin/index/index', '', 1, 'auth', 1);

-- --------------------------------------------------------

--
-- 表的结构 `osc_config`
--

CREATE TABLE `osc_config` (
  `id` int(10) UNSIGNED NOT NULL COMMENT '配置ID',
  `name` varchar(100) DEFAULT NULL COMMENT '配置名称',
  `value` text COMMENT '配置值',
  `info` varchar(255) DEFAULT NULL COMMENT '描述',
  `module` varchar(40) DEFAULT NULL COMMENT '所属模块',
  `extend_value` varchar(255) DEFAULT NULL COMMENT '扩展值',
  `use_for` varchar(32) DEFAULT NULL COMMENT '用于',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态，1启用，0禁用',
  `sort_order` int(5) NOT NULL DEFAULT '0' COMMENT '排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `osc_config`
--

INSERT INTO `osc_config` (`id`, `name`, `value`, `info`, `module`, `extend_value`, `use_for`, `status`, `sort_order`) VALUES
(13, 'SITE_TITLE', 'oscshop', '', 'common', '', '', 1, 0),
(14, 'SITE_NAME', 'oscshop', '', 'common', '', '', 1, 0),
(17, 'SITE_URL', 'o2.com', '', 'common', '', '', 1, 0),
(172, 'administrator', 'admin', NULL, 'common', NULL, NULL, 1, 0),
(185, 'page_num', '10', NULL, 'common', NULL, NULL, 1, 0);

--
-- 转储表的索引
--

--
-- 表的索引 `osc_admin`
--
ALTER TABLE `osc_admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- 表的索引 `osc_admin_auth_group`
--
ALTER TABLE `osc_admin_auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- 表的索引 `osc_admin_auth_group_access`
--
ALTER TABLE `osc_admin_auth_group_access`
  ADD UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `group_id` (`group_id`);

--
-- 表的索引 `osc_admin_auth_rule`
--
ALTER TABLE `osc_admin_auth_rule`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `osc_admin_menu`
--
ALTER TABLE `osc_admin_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pid` (`pid`);

--
-- 表的索引 `osc_config`
--
ALTER TABLE `osc_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_name` (`name`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `osc_admin`
--
ALTER TABLE `osc_admin`
  MODIFY `admin_id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- 使用表AUTO_INCREMENT `osc_admin_auth_group`
--
ALTER TABLE `osc_admin_auth_group`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `osc_admin_auth_rule`
--
ALTER TABLE `osc_admin_auth_rule`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键', AUTO_INCREMENT=80;

--
-- 使用表AUTO_INCREMENT `osc_admin_menu`
--
ALTER TABLE `osc_admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文档ID', AUTO_INCREMENT=446;

--
-- 使用表AUTO_INCREMENT `osc_config`
--
ALTER TABLE `osc_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '配置ID', AUTO_INCREMENT=187;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
